import React from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as yup from 'yup';
import './CustomerInfoForm.scss';
import { connect } from 'react-redux';
import actions from '../../store/actions';
import InputField from './InputField';

const schema = yup.object().shape({
  firstName: yup.string().required('name is required'),
  lastName: yup.string().required('last name is required'),
  age: yup.number('please provide a number')
    .required('age is required')
    .positive('please provide a number')
    .integer('please provide a number')
    .min(18, 'you must be of age'),
  address: yup.string().required('address is required'),
  phone: yup.number('please provide a number')
    .required('phone is required')
    .positive('please provide a number')
    .integer('please provide a number')
});

function CustomerInfoForm({ itemsInCart, clearCart }) {
  const handleSubmit = (values) => {
    console.log(itemsInCart);
    localStorage.removeItem('itemsInCart');
    clearCart();
    console.log(JSON.stringify(values, null, 2));
  }

  return (
    <Formik
      initialValues={{
        firstName: '',
        lastName: '',
        age: null,
        address: '',
        phone: null
      }}
      validationSchema={schema}
      validateOnBlur
      onSubmit={(values) => handleSubmit(values)}
    >
      {({ values, handleChange }) => (
        <Form className="form-container">
          <InputField label="Name" name="firstName" value={values.firstName} type="text" placeholder="Jane" onChange={handleChange} />

          <InputField label="Last Name" name="lastName" value={values.lastName} type="text" placeholder="Doe" onChange={handleChange} />

          <InputField label="Age" name="age" value={values.age} type="number" placeholder="32" onChange={handleChange} />

          <InputField label="Address" name="address" value={values.address} type="text" placeholder="Somewhere Street 0" onChange={handleChange} />

          <InputField label="Phone" name="phone" value={values.phone} type="string" placeholder="111-333-444-55" onChange={handleChange} />
          <button className="submit-btn" type="submit">Checkout</button>
        </Form>
      )}
    </Formik>
  )
}

const mapStateToProps = (state) => {
  return {
    itemsInCart: state.itemsInCart
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    clearCart: () => dispatch(actions.setItemsInCart([]))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(CustomerInfoForm)
