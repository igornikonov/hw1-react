import React from 'react';
import './Header.scss';
import headerIcon2 from './img/header-icon2.png';
import headerIcon3 from './img/header-icon3.png';
import headerIcon4 from './img/header-icon4.png';
import headerIcon5 from './img/header-icon5.png';
import headerIcon6 from './img/header-icon6.png';
import headerIcon7 from './img/header-icon7.png';
import headerIcon8 from './img/header-icon8.png';
import headerIcon9 from './img/header-icon9.png';
import { NavLink } from 'react-router-dom';

const iconPaths = [headerIcon3, headerIcon2, headerIcon5, headerIcon4, headerIcon7, headerIcon6, headerIcon8, headerIcon9];
const headerIcons = iconPaths.map((path, i) => (
  <img key={i} src={path} alt='a bycicle' className='header-icon' />
));

function Header() {
  return (
    <>
      <div className='header__icons-container'>
        {headerIcons}
      </div>
      <ul className='header__nav-links-container'>
        <li><NavLink className="nav-link" exact to='/'>Catalogue</NavLink></li>
        <li><NavLink className="nav-link" to='/favourites'>Favourites</NavLink></li>
        <li><NavLink className="nav-link" to='Cart'>Cart</NavLink></li>
      </ul>
    </>
  );
}

export default Header;