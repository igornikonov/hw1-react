import React, { Component } from 'react';
import './App.scss';
import Button from './Components/Button/Button';
import Modal from './Components/Modal/Modal';
import { modalWindowDeclarations } from './ModalConfig'

class App extends Component {
  state = {
    onScreen: 'btns',
    modalContent: {}
  };
  openModalHandler(e) {
    const modalID = e.target.id;
    const modalDeclaration = modalWindowDeclarations.find(item => item.id === modalID);
    this.setState({
      onScreen: 'modal',
      modalContent: modalDeclaration
    });
  }
  closeModalHandler(e) {
    if (e.target.classList.contains("modal-container") ||
      e.target.classList.contains("modal__header__cross") ||
      e.target.classList.contains("close")) {
      this.setState({
        onScreen: 'btns'
      });
    }
  }

  render() {
    if (this.state.onScreen === 'btns') {
      return (
        <div className="container">
          <div className="btnContainer">
            <Button
              btnID="modalID1"
              text="Open first modal"
              backgroundColor="rgba(255, 238, 0, 0.925)"
              onClick={e => this.openModalHandler(e)}
            />
            <Button
              btnID="modalID2"
              text="Open second modal"
              backgroundColor="aquamarine"
              onClick={e => this.openModalHandler(e)}
            />
          </div>
        </div>
      );
    }
    return (
      <div className="container">
        <Modal
          header={this.state.modalContent.title}
          text={this.state.modalContent.description}
          backgroundColor={this.state.modalContent.backgroundColor}
          headerBackgroundColor={this.state.modalContent.headerBackgroundColor}
          crossBtn={this.state.modalContent.crossBtn}
          closeModal={e => this.closeModalHandler(e)}
          actions={
            <div className="modal__btn-container">
              <button className="modal__btn-container__btn">Ok</button>
              <button className="modal__btn-container__btn close" onClick={e => this.closeModalHandler(e)}>Cancel</button>
            </div>
          }
        />
      </div>
    )
  }
}

export default App;
