import React, { Component } from 'react';
import './Button.scss';

class Button extends Component {

  render() {
    const { text, backgroundColor, onClick, btnID } = this.props;

    return (
      <button
        id={btnID}
        className="btn"
        style={{ backgroundColor: backgroundColor }}
        onClick={onClick}
      >{text}</button>
    )
  }
}

export default Button;