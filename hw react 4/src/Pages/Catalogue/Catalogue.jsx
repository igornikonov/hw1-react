import React from 'react';
import Modal from '../../Components/Modal/Modal';
import './Catalogue.scss';
import { connect } from 'react-redux';
import operations from '../../store/operations'

function Catalogue({ noModals, renderedShopItems, modalContent, closeModal, addToCart, currentItemId }) {
  const { title, description, backgroundColor, headerBackgroundColor } = modalContent;

  if (noModals) {
    return (
      <ul className="shop-items-container">
        {renderedShopItems}
      </ul>
    );
  }
  return (
    <>
      <Modal
        header={title}
        text={description}
        backgroundColor={backgroundColor}
        headerBackgroundColor={headerBackgroundColor}
        closeModal={closeModal}
        action={addToCart}
        id={currentItemId}
      />
    </>
  )
}

function mapStateToProps(state) {
  return {
    noModals: state.noModals,
    modalContent: state.modalContent,
    currentItemId: state.currentItemId,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    closeModal: () => dispatch(operations.closeModal()),
    addToCart: (id) => dispatch(operations.addToCart(id))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Catalogue);
