import React, { Component } from 'react';
import './Button.scss';

class Button extends Component {

  render() {
    const { text, backgroundColor, onClick, show } = this.props;

    if (!show) return null;
    return (
      <button
        className="btn"
        style={{ backgroundColor: backgroundColor }}
        onClick={onClick}
      >{text}</button>
    )
  }
}

export default Button;