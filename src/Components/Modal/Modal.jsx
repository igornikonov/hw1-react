import React, { Component } from 'react';
import './Modal.scss';

class Modal extends Component {
  render() {
    const { show, header, closeBtn, actions, hideModal, text, backgroundColor, headerBackgroundColor } = this.props;

    if (!show) return null;
    return (
      <div className="modal-container"
        onClick={e => hideModal(e)}>
        <div className="modal" style={{ backgroundColor: backgroundColor }}>
          <div className="modal__header" style={{ backgroundColor: headerBackgroundColor }}>
            <h2 className="modal__header__text">{header}</h2>
            {
              closeBtn === 'true' && <span className="modal__header__cross" onClick={e => hideModal(e)}>X</span>
            }
          </div>
          <p className="modal__text">{text}</p>
          {actions}
        </div>
      </div>
    );
  }
}

export default Modal;