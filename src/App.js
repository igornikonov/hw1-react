import React, { Component } from 'react';
import './App.scss';
import Button from './Components/Button/Button';
import Modal from './Components/Modal/Modal';

class App extends Component {
  state = {
    showModal1: false,
    showModal2: false,
    showBtns: true
  };
  showModal1Handler = e => {
    this.setState({
      showModal1: true,
      showBtns: false
    });
  };
  showModal2Handler = e => {
    this.setState({
      showModal2: true,
      showBtns: false
    });
  };
  hideModalHandler = e => {
    if (e.target.classList.contains('modal-container')
      || e.target.classList.contains('close')
      || e.target.classList.contains('modal__header__cross')) {
      this.setState({
        showModal1: false,
        showModal2: false,
        showBtns: true
      });
    }
  }


  render() {
    const firstBtnHeader = "Create new piece of art?";
    const firstBtnText = "Once you create a new piece of art, you will have the responsibility for the generations that will be led by it for years ahead"
    const secondBtnHeader = "Do you want to delete this file?";
    const secondBtnText = "Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?";

    return (
      <div className="container">
        <div className="btnContainer">
          <Button
            text="Open first modal"
            backgroundColor="rgba(255, 238, 0, 0.925)"
            show={this.state.showBtns}
            onClick={e => this.showModal1Handler()}
          />
          <Button
            text="Open second modal"
            backgroundColor="aquamarine"
            show={this.state.showBtns}
            onClick={e => this.showModal2Handler()}
          />
        </div>
        <Modal
          show={this.state.showModal1}
          header={firstBtnHeader}
          text={firstBtnText}
          closeBtn="false"
          headerBackgroundColor="rgba(57, 17, 121, 0.747)"
          backgroundColor="rgba(17, 56, 128, 0.747)"
          hideModal={e => this.hideModalHandler(e)}
          actions={
            <div className="modal__btn-container">
              <button className="modal__btn-container__btn">Ok</button>
              <button className="modal__btn-container__btn close" onClick={e => this.hideModalHandler(e)}>Cancel</button>
            </div>
          }
        />
        <Modal
          show={this.state.showModal2}
          header={secondBtnHeader}
          text={secondBtnText}
          closeBtn="true"
          headerBackgroundColor="rgba(175, 27, 27, 0.678)"
          backgroundColor="rgb(226, 7, 7)"
          hideModal={e => this.hideModalHandler(e)}
          actions={
            <div className="modal__btn-container">
              <button className="modal__btn-container__btn">Ok</button>
              <button className="modal__btn-container__btn close" onClick={e => this.hideModalHandler(e)}>Cancel</button>
            </div>
          }
        />
      </div>
    );
  }
}

export default App;
