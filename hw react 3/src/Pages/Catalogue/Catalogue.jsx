import React from 'react';
import Modal from '../../Components/Modal/Modal';
import './Catalogue.scss';

function Catalogue({ noModals, renderedShopItems, modalContent, closeModal, addToCart, currentItemId }) {
  const { title, description, backgroundColor, headerBackgroundColor } = modalContent;

  if (noModals) {
    return (
      <ul className="shop-items-container">
        {renderedShopItems}
      </ul>
    );
  }
  return (
    <Modal
      header={title}
      text={description}
      backgroundColor={backgroundColor}
      headerBackgroundColor={headerBackgroundColor}
      closeModal={closeModal}
      action={addToCart}
      id={currentItemId}
    />
  )
}

export default Catalogue;
