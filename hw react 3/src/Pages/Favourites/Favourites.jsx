import React from 'react';
import './Favourites.scss';
import Modal from '../../Components/Modal/Modal';

function Favourites({ favourites, renderedShopItems, noModals, closeModal, addToCart, currentItemId, modalContent }) {

  const { title, description, backgroundColor, headerBackgroundColor } = modalContent;

  const itemsInFavourites = [];
  favourites.forEach(favItem => {
    const currentFav = renderedShopItems.find(item => item.props.id === +favItem);
    itemsInFavourites.push(currentFav);
  });

  if (noModals) {
    if (favourites.length > 0) {
      return (
        <ul className="favourites-items-container">
          {itemsInFavourites}
        </ul>
      )
    }
    return (
      <h1 className="empty-favourites">There are on items in favourites</h1>
    )
  }
  return (
    <Modal
      header={title}
      text={description}
      backgroundColor={backgroundColor}
      headerBackgroundColor={headerBackgroundColor}
      closeModal={closeModal}
      action={addToCart}
      id={currentItemId}
    />
  )

}

export default Favourites
