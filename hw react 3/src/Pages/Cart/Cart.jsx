import React from 'react';
import ItemCard from '../../Components/ItemCard/ItemCard';
import Modal from '../../Components/Modal/Modal';
import './Cart.scss';

function Cart({ itemsInCart, removeFromCart, noModals, currentItemId, modalContent, closeModal, openModal }) {
  const renderedCartItems = itemsInCart.map(({ name, id, brand, picture, price }) =>
    <ItemCard
      key={id}
      id={id}
      name={name}
      brand={brand}
      picture={picture}
      price={price}
      openModal={openModal}
    />
  )
  const { title, description, backgroundColor, headerBackgroundColor } = modalContent;

  if (noModals) {
    if (itemsInCart.length > 0) {
      return (
        <ul className="cart-items-container">
          {renderedCartItems}
        </ul>
      )
    }
    return (
      <h1 className="empty-cart">There are on items in cart</h1>
    )
  }
  return (
    <Modal
      header={title}
      text={description}
      backgroundColor={backgroundColor}
      headerBackgroundColor={headerBackgroundColor}
      closeModal={closeModal}
      action={removeFromCart}
      id={currentItemId}
    />
  )

}

export default Cart
