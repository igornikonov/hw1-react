import React from 'react';
import './ItemCard.scss';
import Button from '../Button/Button';
import Star from '../Star/Star';
import propTypes from 'prop-types';

function ItemCard({ name, picture, brand, price, openModal, id, addToFavourites, favourites, notInCart }) {
  return (
    <>
      <li className="item" id={id}>
        <h2>{name}</h2>
        {notInCart &&
          <Star
            onClick={() => addToFavourites(id)}
            filled={favourites.includes(id.toString())}
          />
        }
        {!notInCart &&
          <span id="delete-item" className="remove-from-cart" onClick={() => openModal("delete-item", id)}>X</span>
        }
        <img src={picture} alt="shop item" className="item__image" />
        <h3>{brand}</h3>
        <p>{price}</p>
        {notInCart &&
          <Button
            className="btn"
            text="Add to cart"
            id="add-to-cart"
            onClick={() => openModal("add-to-cart", id)}
          />}
      </li>
    </>
  )
}

ItemCard.propTypes = {
  name: propTypes.string.isRequired,
  picture: propTypes.string.isRequired,
  brand: propTypes.string.isRequired,
  price: propTypes.oneOfType([propTypes.string, propTypes.number]),
  openModal: propTypes.func.isRequired,
  id: propTypes.number.isRequired
}

ItemCard.defaultProps = {
  price: 'please look up the price in the item description'
}

export default ItemCard