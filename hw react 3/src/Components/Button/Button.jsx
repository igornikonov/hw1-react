import React from 'react';
import './Button.scss';
import propTypes from 'prop-types';

function Button({ text, onClick, id, className }) {
  return (
    <button
      id={id}
      className={className}
      onClick={onClick}
    >
      {text}
    </button>
  )
}

Button.propTypes = {
  text: propTypes.string.isRequired,
  onClick: propTypes.func.isRequired,
  className: propTypes.string,
  btnID: propTypes.string.isRequired
}

Button.defaultProps = {
  className: 'btn',
  btnID: ''
}

export default Button;