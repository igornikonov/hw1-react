import React, { useState, useEffect } from 'react';
import { modalWindowDeclarations } from './Components/Modal/ModalConfig';
import Catalogue from './Pages/Catalogue/Catalogue';
import Cart from './Pages/Cart/Cart';
import Favourites from './Pages/Favourites/Favourites';
import Header from './Components/Header/Header'
import ItemCard from './Components/ItemCard/ItemCard';
import { Route, Switch } from 'react-router-dom';

function App() {
  const [noModals, setNoModals] = useState(true);
  const [shopItems, setShopItems] = useState([]);
  const [currentItemId, setCurrentItemId] = useState(null);
  const [itemsInCart, setItemsInCart] = useState(JSON.parse(localStorage.getItem('itemsInCart')) || []);
  const [favourites, setFavourites] = useState(JSON.parse(localStorage.getItem('favourites')) || []);
  const [modalContent, setModalContent] = useState({});

  useEffect(() => {
    fetch('shopItems.json')
      .then(res => res.json())
      .then(data => {
        setShopItems(data)
      });
  }, []);

  const openModalHandler = (modalID, itemID) => {
    const modalDeclaration = modalWindowDeclarations.find(item => item.id === modalID);

    setNoModals(!noModals);
    setModalContent(modalDeclaration);
    setCurrentItemId(itemID);
  };

  const closeModalHandler = () => {
    setNoModals(!noModals);
    setModalContent({});
    setCurrentItemId(null);
  };

  const addToCartHandler = (id) => {
    const cartItems = JSON.parse(localStorage.getItem('itemsInCart')) || [];
    const itemToAdd = shopItems.find(item => item.id.toString() === id.toString());
    cartItems.push(itemToAdd);

    localStorage.setItem('itemsInCart', JSON.stringify(cartItems));
    setItemsInCart(cartItems);

    closeModalHandler();
  };

  const removeFromCartHandler = (id) => {
    const cartItems = JSON.parse(localStorage.getItem('itemsInCart')) || [];

    for (let i = 0; i < cartItems.length; i++) {
      if (cartItems[i].id === id) {
        cartItems.splice(i, 1);
      }
    }


    localStorage.setItem('itemsInCart', JSON.stringify(cartItems));
    setItemsInCart(cartItems);

    closeModalHandler();
  }

  const addToFavouritesHandler = (id) => {
    const favouriteItems = JSON.parse(localStorage.getItem('favourites')) || [];

    if (!favouriteItems.includes(id.toString())) {
      favouriteItems.push(id.toString());
    } else {
      for (let i = 0; i < favouriteItems.length; i++) {
        if (favouriteItems[i] === id.toString()) {
          favouriteItems.splice(i, 1);
        }
      }
    }

    localStorage.setItem('favourites', JSON.stringify(favouriteItems));
    setFavourites(favouriteItems);
  }

  const renderedShopItems = shopItems.map(({ name, id, brand, picture, price }) =>
    <ItemCard
      key={id}
      id={id}
      name={name}
      brand={brand}
      picture={picture}
      price={price}
      addToFavourites={addToFavouritesHandler}
      openModal={openModalHandler}
      favourites={favourites}
      notInCart
    />
  )

  return (
    <>
      <Header />
      <Switch>
        <Route exact path='/'>
          <Catalogue
            noModals={noModals}
            renderedShopItems={renderedShopItems}
            modalContent={modalContent}
            closeModal={closeModalHandler}
            addToCart={addToCartHandler}
            currentItemId={currentItemId}
          />
        </Route>
        <Route exact path='/cart'>
          <Cart
            itemsInCart={itemsInCart}
            removeFromCart={removeFromCartHandler}
            noModals={noModals}
            currentItemId={currentItemId}
            modalContent={modalContent}
            closeModal={closeModalHandler}
            openModal={openModalHandler}
          />
        </Route>
        <Route exact path='/favourites'>
          <Favourites
            shopItems={shopItems}
            favourites={favourites}
            renderedShopItems={renderedShopItems}
            closeModal={closeModalHandler}
            addToCart={addToCartHandler}
            modalContent={modalContent}
            noModals={noModals}
            currentItemId={currentItemId}
          />
        </Route>
      </Switch>
    </>
  )
}

export default App;