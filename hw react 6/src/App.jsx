import React, { useEffect } from 'react';
import Catalogue from './Pages/Catalogue/Catalogue';
import Cart from './Pages/Cart/Cart';
import Favourites from './Pages/Favourites/Favourites';
import Header from './Components/Header/Header'
import ItemCard from './Components/ItemCard/ItemCard';
import { Route, Switch } from 'react-router-dom';
import operations from './store/operations';
import { connect } from 'react-redux';

function App({ fetchShopItems, shopItems, openModal, addToFavourites, favourites, itemsInCart }) {
  useEffect(() => {
    fetchShopItems();
  }, [fetchShopItems]);

  const renderedShopItems = shopItems.map(({ name, id, brand, picture, price }) =>
    <ItemCard
      key={id}
      id={id}
      name={name}
      brand={brand}
      picture={picture}
      price={price}
      addToFavourites={addToFavourites}
      openModal={openModal}
      favourites={favourites}
      notInCart
    />
  )

  return (
    <>
      <Header />
      <Switch>
        <Route exact path='/'>
          <Catalogue renderedShopItems={renderedShopItems} />
        </Route>
        <Route exact path='/cart'>
          <Cart renderedShopItems={renderedShopItems} />
        </Route>
        <Route exact path='/favourites'>
          <Favourites renderedShopItems={renderedShopItems} />
        </Route>
      </Switch>
    </>
  )
}

function mapStateToProps(state) {
  return {
    shopItems: state.shopItems,
    favourites: state.favourites,
    itemsInCart: state.itemsInCart
  }
}

function mapDispatchToProps(dispatch) {
  return {
    fetchShopItems: () => dispatch(operations.fetchShopItems()),
    openModal: (modalID, productID) => dispatch(operations.openModal(modalID, productID)),
    addToFavourites: (id) => dispatch(operations.addToFavourites(id))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);