import types from './types';

const setShopItems = (data) => ({ type: types.FETCH_SHOP_ITEMS, payload: data });

const toggleNoModals = (data) => ({ type: types.TOGGLE_NO_MODALS, payload: data });

const setCurrentItemId = (data) => ({ type: types.SET_CURRENT_ITEM_ID, payload: data });

const setItemsInCart = (data) => ({ type: types.SET_ITEMS_IN_CART, payload: data });

const setFavourites = (data) => ({ type: types.SET_FAVOURITES, payload: data });

const setModalContent = (data) => ({ type: types.SET_MODAL_CONTENT, payload: data });

const actions = {
  setShopItems,
  toggleNoModals,
  setCurrentItemId,
  setItemsInCart,
  setFavourites,
  setModalContent
}

export default actions;