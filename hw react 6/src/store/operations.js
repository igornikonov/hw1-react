import actions from './actions';
import { modalWindowDeclarations } from "../Components/Modal/ModalConfig";

const fetchShopItems = () => (dispatch, getState) => {
  fetch('shopItems.json')
    .then(res => res.json())
    .then(data => {
      dispatch(actions.setShopItems(data))
    });
}

const openModal = (modalID, productID) => (dispatch, getState) => {
  const modalDeclaration = modalWindowDeclarations.find(item => item.id === modalID);

  dispatch(actions.toggleNoModals(!getState().noModals));
  dispatch(actions.setModalContent(modalDeclaration));
  dispatch(actions.setCurrentItemId(productID));
}

const closeModal = () => (dispatch, getState) => {
  dispatch(actions.toggleNoModals(!getState().noModals));
  dispatch(actions.setModalContent({}));
  dispatch(actions.setCurrentItemId(null));
};

const addToCart = (id) => (dispatch, getState) => {
  const cartItems = JSON.parse(localStorage.getItem('itemsInCart')) || [];
  const itemToAdd = getState().shopItems.find(item => item.id === id);
  cartItems.push(itemToAdd);

  localStorage.setItem('itemsInCart', JSON.stringify(cartItems));
  dispatch(actions.setItemsInCart(cartItems));

  dispatch(closeModal());
};

const removeFromCart = (id) => (dispatch, getState) => {
  const cartItems = JSON.parse(localStorage.getItem('itemsInCart')) || [];

  for (let i = 0; i < cartItems.length; i++) {
    if (cartItems[i].id === id) {
      cartItems.splice(i, 1);
    }
  }

  localStorage.setItem('itemsInCart', JSON.stringify(cartItems));
  dispatch(actions.setItemsInCart(cartItems));

  dispatch(closeModal());
}

const addToFavourites = (id) => (dispatch, getState) => {
  const favouriteItems = JSON.parse(localStorage.getItem('favourites')) || [];

  if (!favouriteItems.includes(id.toString())) {
    favouriteItems.push(id.toString());
  } else {
    for (let i = 0; i < favouriteItems.length; i++) {
      if (favouriteItems[i] === id.toString()) {
        favouriteItems.splice(i, 1);
      }
    }
  }

  localStorage.setItem('favourites', JSON.stringify(favouriteItems));
  dispatch(actions.setFavourites(favouriteItems));
}

const operations = {
  fetchShopItems,
  openModal,
  closeModal,
  addToCart,
  addToFavourites,
  removeFromCart
}

export default operations;