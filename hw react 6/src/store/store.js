import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import types from './types';

const initialState = {
  noModals: true,
  shopItems: [],
  currentItemId: null,
  itemsInCart: JSON.parse(localStorage.getItem('itemsInCart')) || [],
  favourites: JSON.parse(localStorage.getItem('favourites')) || [],
  modalContent: {}
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.FETCH_SHOP_ITEMS: {
      return { ...state, shopItems: action.payload }
    }
    case types.TOGGLE_NO_MODALS: {
      return { ...state, noModals: action.payload }
    }
    case types.SET_CURRENT_ITEM_ID: {
      return { ...state, currentItemId: action.payload }
    }
    case types.SET_ITEMS_IN_CART: {
      return { ...state, itemsInCart: action.payload }
    }
    case types.SET_FAVOURITES: {
      return { ...state, favourites: action.payload }
    }
    case types.SET_MODAL_CONTENT: {
      return { ...state, modalContent: action.payload }
    }
    default: return state;
  }
};

const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk)));

export default store;