import React from 'react';
import styles from './Favourites.module.scss';
import Modal from '../../Components/Modal/Modal';
import { connect } from 'react-redux';
import operations from '../../store/operations'

function Favourites({ favourites, renderedShopItems, noModals, closeModal, addToCart, currentItemId, modalContent }) {
  const { title, description, backgroundColor, headerBackgroundColor } = modalContent;

  const itemsInFavourites = [];
  favourites.forEach(favItem => {
    const currentFav = renderedShopItems.find(item => item.props.id === +favItem);
    itemsInFavourites.push(currentFav);
  });

  if (favourites.length > 0) {
    return (
      <div>
        <ul className={styles["favourites-items-container"]}>
          {itemsInFavourites}
        </ul>
        {!noModals && 
          <Modal
            header={title}
            text={description}
            backgroundColor={backgroundColor}
            headerBackgroundColor={headerBackgroundColor}
            closeModal={closeModal}
            action={addToCart}
            id={currentItemId}
          />
        }
      </div>
    )
  }
  return <h1 className={styles["empty-favourites"]}>There are no items in favourites</h1>
}

function mapStateToProps(state) {
  return {
    favourites: state.favourites,
    noModals: state.noModals,
    modalContent: state.modalContent,
    currentItemId: state.currentItemId
  }
}

function mapDispatchToProps(dispatch) {
  return {
    closeModal: () => dispatch(operations.closeModal()),
    addToCart: (id) => dispatch(operations.addToCart(id))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Favourites)
