import React from 'react';
import Modal from '../../Components/Modal/Modal';
import styles from './Catalogue.module.scss';
import { connect } from 'react-redux';
import operations from '../../store/operations'

function Catalogue({ noModals, renderedShopItems, modalContent, closeModal, addToCart, currentItemId }) {
  const { title, description, backgroundColor, headerBackgroundColor } = modalContent;

  return (
    <div>
      <ul className={styles["shop-items-container"]}>
        {renderedShopItems}
      </ul>
      {!noModals && 
        <Modal
          header={title}
          text={description}
          backgroundColor={backgroundColor}
          headerBackgroundColor={headerBackgroundColor}
          closeModal={closeModal}
          action={addToCart}
          id={currentItemId}
        />
      }
    </div>
  );
}

function mapStateToProps(state) {
  return {
    noModals: state.noModals,
    modalContent: state.modalContent,
    currentItemId: state.currentItemId,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    closeModal: () => dispatch(operations.closeModal()),
    addToCart: (id) => dispatch(operations.addToCart(id))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Catalogue);
