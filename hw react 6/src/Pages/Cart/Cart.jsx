import React from 'react';
import ItemCard from '../../Components/ItemCard/ItemCard';
import Modal from '../../Components/Modal/Modal';
import styles from './Cart.module.scss';
import { connect } from 'react-redux';
import operations from '../../store/operations';
import CustomerInfoForm from '../../Components/CustomerInfoForm/CustomerInfoForm';

function Cart({ itemsInCart, removeFromCart, noModals, currentItemId, modalContent, openModal, closeModal }) {
  const renderedCartItems = itemsInCart.map(({ name, id, brand, picture, price }) =>
    <ItemCard
      key={id}
      id={id}
      name={name}
      brand={brand}
      picture={picture}
      price={price}
      openModal={openModal}
    />
  );
  const { title, description, backgroundColor, headerBackgroundColor } = modalContent;

  if (itemsInCart.length > 0) {
    return (
      <div className={styles['container']}>
        <CustomerInfoForm />
        <ul className={styles["cart-items-container"]}>
          {renderedCartItems}
        </ul>
        {!noModals && 
          <Modal
            header={title}
            text={description}
            backgroundColor={backgroundColor}
            headerBackgroundColor={headerBackgroundColor}
            closeModal={closeModal}
            action={removeFromCart}
            id={currentItemId}
          />
        }
      </div>
    )
  }
  return (
    <h1 className={styles["empty-cart"]}>Cart is empty</h1>
  )
}

function mapStateToProps(state) {
  return {
    itemsInCart: state.itemsInCart,
    noModals: state.noModals,
    currentItemId: state.currentItemId,
    modalContent: state.modalContent
  }
}

function mapDispatchToProps(dispatch) {
  return {
    removeFromCart: (id) => dispatch(operations.removeFromCart(id)),
    openModal: (modalID, productID) => dispatch(operations.openModal(modalID, productID)),
    closeModal: () => dispatch(operations.closeModal())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Cart)
