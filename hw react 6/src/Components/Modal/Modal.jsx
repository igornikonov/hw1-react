import React from 'react';
import './Modal.scss';
import Button from '../Button/Button';
import propTypes from 'prop-types';

function Modal({ header, closeModal, text, id, action }) {
  const actions = (
    <div className="modal__btn-container">
      <Button
        className="modal-btn"
        text="OK"
        onClick={() => action(id)}
      />
      <Button className="modal-btn" text="Close" onClick={closeModal} />
    </div>
  );

  return (
    <div className="modal-container" onClick={closeModal}>
      <div className="modal" onClick={(e) => e.stopPropagation()}>
        <div className="modal__header">
          <h2 className="modal__header__text" data-testid="modal-header">{header}</h2>
          <span
            className="modal__header__cross"
            onClick={closeModal}
            data-testid="modal-span-cross"
          >
            X
          </span>
        </div>
        <p className="modal__text" data-testid="modal-text">{text}</p>
        {actions}
      </div>
    </div>
  );
}

Modal.propTypes = {
  header: propTypes.oneOf(['Add this item to the cart?', 'Do you want to delete this item?']).isRequired,
  closeModal: propTypes.func.isRequired,
  text: propTypes.oneOf(['Once you add this item, you can view it in the cart.',
    "Once you delete this item, it won't be possible to undo this action. Are you sure you want to delete it?"]).isRequired,
  action: propTypes.func.isRequired,
  id: propTypes.number.isRequired
}

export default Modal;