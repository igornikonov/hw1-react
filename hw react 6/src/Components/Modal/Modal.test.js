import Modal from "./Modal";
import { render } from '@testing-library/react';

test('whether the modal header has its content', () => {
  const { getByTestId } = render(<Modal header='test header content'></Modal>);
  expect(getByTestId('modal-header')).toHaveTextContent('test header content');
});

test('whether the modal text has its content', () => {
  const { getByTestId } = render(<Modal text='test text content'></Modal>);
  expect(getByTestId('modal-text')).toHaveTextContent('test text content');
});

test('whether the span element "cross" has the className "modal__header__cross"', () => {
  const { getByTestId } = render(<Modal></Modal>);
  expect(getByTestId('modal-span-cross')).toHaveClass('modal__header__cross');
});