import React from 'react';
import { useField } from 'formik';
import './CustomerInfoForm.scss';

function InputField({ label, ...props }) {
  const [field, meta] = useField(props);

  return (
    <>
      <label className="label" htmlFor={props.name}>{label}</label>
      <input className="input-field" {...field} {...props} value={undefined} />
      {meta.touched && meta.error && <div className="error">{meta.error}</div>}
    </>
  )
}

export default InputField;