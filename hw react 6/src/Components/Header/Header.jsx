import React from 'react';
import styles from './Header.module.scss';
import headerIcon2 from './img/header-icon2.png';
import headerIcon3 from './img/header-icon3.png';
import headerIcon4 from './img/header-icon4.png';
import headerIcon5 from './img/header-icon5.png';
import headerIcon6 from './img/header-icon6.png';
import headerIcon7 from './img/header-icon7.png';
import headerIcon8 from './img/header-icon8.png';
import headerIcon9 from './img/header-icon9.png';
import { NavLink } from 'react-router-dom';

const iconPaths = [headerIcon3, headerIcon2, headerIcon5, headerIcon4, headerIcon7, headerIcon6, headerIcon8, headerIcon9];
const headerIcons = iconPaths.map((path, i) => (
  <img key={i} src={path} alt='a bycicle' className={styles['header-icon']} />
));

function Header() {
  return (
    <>
      <div className={styles['header__icons-container']}>
        {headerIcons}
      </div>
      <ul className={styles['header__nav-links-container']}>
        <li>
          <NavLink className={styles["nav-link"]} activeClassName={styles['active']} exact to='/'>
            Catalogue
          </NavLink>
        </li>
        <li>
          <NavLink className={styles["nav-link"]} activeClassName={styles['active']} to='/favourites'>
            Favourites
          </NavLink>
        </li>
        <li>
          <NavLink className={styles["nav-link"]} activeClassName={styles['active']} to='Cart'>
            Cart
          </NavLink>
        </li>
      </ul>
    </>
  );
}

export default Header;