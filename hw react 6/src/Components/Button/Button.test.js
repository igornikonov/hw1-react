import Button from "./Button";
import { render } from '@testing-library/react';

describe('testing the "Add-to-cart" button in the ItemCard component', () => {

  test('whether the "Add-to-cart" button has its content', () => {
    const { getByTestId } = render(<Button text='Add to cart'></Button>);
    expect(getByTestId('button-test')).toHaveTextContent('Add to cart');
  });

  test('whether the "Add-to-cart" button has the className "btn"', () => {
    const { getByTestId } = render(<Button className='btn'></Button>);
    expect(getByTestId('button-test')).toHaveClass('btn');
  });
});

describe('testing the buttons in the Modal component', () => {

  test('whether the "OK" button has its content', () => {
    const { getByTestId } = render(<Button text='OK'></Button>);
    expect(getByTestId('button-test')).toHaveTextContent('OK');
  });

  test('whether the "Close" button has its content', () => {
    const { getByTestId } = render(<Button text='Close'></Button>);
    expect(getByTestId('button-test')).toHaveTextContent('Close');
  });

  test('whether the "OK" and "Close" buttons have the className "modal-btn"', () => {
    const { getByTestId } = render(<Button className='modal-btn'></Button>);
    expect(getByTestId('button-test')).toHaveClass('modal-btn');
  });
});