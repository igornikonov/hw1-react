import React from 'react';
import './Button.scss';
import propTypes from 'prop-types';

function Button({ text, onClick, className }) {
  return (
    <button
      className={className}
      onClick={onClick}
      data-testid="button-test"
    >
      {text}
    </button>
  )
}

Button.propTypes = {
  text: propTypes.string.isRequired,
  onClick: propTypes.func.isRequired,
  className: propTypes.string,
}

Button.defaultProps = {
  className: 'btn'
}

export default Button;